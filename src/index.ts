import {BigNumber, ethers, providers, Wallet} from 'ethers';
import {TransactionResponse} from '@ethersproject/abstract-provider';
import abi from './abis/loop.json';

const privateKey = 'xxx';
const rpc = 'https://rpc-mumbai.maticvigil.com';
const jsonProvider = new ethers.providers.JsonRpcProvider(rpc);
const wallet = new Wallet(privateKey, jsonProvider);
const contractAddress = '0x49e9C919956732ef6Fd09479Da5606ABF877ea71';

async function cancelTx(
  hash: string,
  maxPriorityFeePerGas: number,
  gasLimit: number
) {
  try {
    const running = await jsonProvider.getTransaction(hash);
    const tx = {
      nonce: running.nonce,
      to: ethers.constants.AddressZero,
      data: '0x',
      gasLimit: gasLimit,
      maxPriorityFeePerGas: maxPriorityFeePerGas,
    };
    const tr = await wallet.sendTransaction(tx);
    console.log(tr.hash);
    await tr.wait();
    console.log('cancel success');
    return true;
  } catch (e) {
    console.log(e);
    return false;
  }
}

async function startHulkTx() {
  const contract = new ethers.Contract(contractAddress, abi, wallet);
  const tx: TransactionResponse = await contract.run();
  return tx;
}

async function tryCancleTx() {
  const value = 5394999999;
  const gasLimit = 18756645;
  // create pending transaction
  const tx = await startHulkTx();
  console.log('running: ', tx.hash);

  // try cancel transaction
  await cancelTx(tx.hash, value, gasLimit);
}

tryCancleTx();
