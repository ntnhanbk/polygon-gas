"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ethers_1 = require("ethers");
const loop_json_1 = __importDefault(require("./abis/loop.json"));
const privateKey = '4a71b4204fc574d2161466f3d025f4e54f18003e8f6c521b1e24925447ce402b';
const rpc = 'https://rpc-mumbai.maticvigil.com';
const jsonProvider = new ethers_1.ethers.providers.JsonRpcProvider(rpc);
const wallet = new ethers_1.Wallet(privateKey, jsonProvider);
const contractAddress = '0x49e9C919956732ef6Fd09479Da5606ABF877ea71';
async function cancelTx(hash, maxPriorityFeePerGas, gasLimit) {
    try {
        const running = await jsonProvider.getTransaction(hash);
        const tx = {
            nonce: running.nonce,
            to: ethers_1.ethers.constants.AddressZero,
            data: '0x',
            gasLimit: gasLimit,
            maxPriorityFeePerGas: maxPriorityFeePerGas,
        };
        const tr = await wallet.sendTransaction(tx);
        console.log(tr.hash);
        await tr.wait();
        console.log('cancel success');
        return true;
    }
    catch (e) {
        console.log(e);
        return false;
    }
}
async function startHulkTx() {
    const contract = new ethers_1.ethers.Contract(contractAddress, loop_json_1.default, wallet);
    const tx = await contract.run();
    return tx;
}
async function tryCancleTx() {
    const value = 5394999999;
    const gasLimit = 18756645;
    // create pending transaction
    const tx = await startHulkTx();
    console.log('running: ', tx.hash);
    // try cancel transaction
    await cancelTx(tx.hash, value, gasLimit);
}
tryCancleTx();
//# sourceMappingURL=index.js.map